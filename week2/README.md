# Odd number filter Lambda Function

This AWS lambda function reads from S3 bucket csv file, which contains a list of numbers and filters those that are odd.

## API Gateway URL:
https://5u4eve4vk5.execute-api.us-east-1.amazonaws.com/default/week2

## Environment requirements:
- Rust 
```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
- Cargo lambda 
``` 
brew tap cargo-lambda/cargo-lambda
brew install cargo-lambda
```
- AWS CLI
- S3 bucket


## Lambda function:

- Reads from S3 bucket named "odd-number-candidates" and from file "odd-number-candidate.csv"
- Parse the content read from .csv file from String into a vector of i32.
- Filter the vector to see which integers are odd numbers and which are not. 

## Configurations:
- Configures the aws config file to include the user information.
- Configure the S3 bucket to allow user access.
- Configure the IAM role attached to the lambda function.

## API Gateway:
In the AWS lambda function console, add a trigger with API Gateway and connect it to the associated lambda function, which in this case is the odd number filter function.

## csv content:
```
numbers
2
3
4
5
6
7
```

## Response:
```
{"odds":[3,5,7]}
```

## Screenshots:
- Gateway screenshot
![Alt text](week2/screenshots/Gateway.png)
- S3 Bucket screenshot
![Alt text](week2/screenshots/S3bucket.png)
- Response screenshot
![Alt text](week2/screenshots/Response.png)
