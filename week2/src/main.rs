use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use lambda_http::{run, service_fn, Body, Error, Request, Response};
use aws_sdk_s3::Client as S3Client;
use serde::{Deserialize, Serialize};
use csv; // Ensure csv crate is included in your Cargo.toml
use serde_json;

#[derive(Serialize, Deserialize, Default)]
struct OddCandidate {
    number: i32,
}

#[derive(Serialize, Deserialize, Default)]
struct OddResponse {
    odds: Vec<i32>,
}

fn is_odd(n: &i32) -> bool {
    let n = *n;
    if n % 2 != 0 {
        return false;
    }
    true
}

/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    let config = aws_config::load_from_env().await; // Adjusted based on deprecation warning
    let client = S3Client::new(&config);

    let bucket = "odd-number-candidate";
    let key = "odd-number-candidates.csv";

    let resp = client.get_object().bucket(bucket).key(key).send().await?;
    let body = resp.body.collect().await?.into_bytes();
    let csv_content = String::from_utf8(body.to_vec()).expect("Found invalid UTF-8");
    println!("{}",csv_content);

    let mut rdr = csv::Reader::from_reader(csv_content.as_bytes());

    let odds: Vec<i32> = rdr.deserialize()
        .filter_map(Result::ok)
        .map(|candidate: OddCandidate| candidate.number)
        .filter(|n| is_odd(n))
        .collect();

    let odd_response = OddResponse { odds };

    Ok(Response::builder()
        .status(200)
        .header("content-type", "application/json")
        .body(serde_json::to_string(&odd_response).unwrap().into())
        .expect("Failed to render response"))

}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
